// <copyright file="WebConfigurationReaderTest.cs" company="SopraGroup">Copyright © SopraGroup 2017</copyright>
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Web;
using CoreLibrary.Common;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreLibrary.Common.Tests
{
    /// <summary>This class contains parameterized unit tests for WebConfigurationReader</summary>
    [PexClass(typeof(WebConfigurationReader))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class WebConfigurationReaderTest
    {
      /// <summary>Test stub for AddAndSaveOneConnectionStringSettings(Configuration, ConnectionStringSettings)</summary>
      [PexMethod(MaxConditions = 10000)]
      public void AddAndSaveOneConnectionStringSettingsTest(
          global::System.Configuration.Configuration configuration,
          ConnectionStringSettings connectionStringSettings
      )
      {
         WebConfigurationReader.AddAndSaveOneConnectionStringSettings
             (configuration, connectionStringSettings);
         // TODO: add assertions to method WebConfigurationReaderTest.AddAndSaveOneConnectionStringSettingsTest(Configuration, ConnectionStringSettings)
         Configuration config = WebConfigurationReader.GetConfiguration();
         Assert.IsNotNull(config.ConnectionStrings.ConnectionStrings[connectionStringSettings.Name]);
         Assert.AreEqual(config.ConnectionStrings.ConnectionStrings[connectionStringSettings.Name].ConnectionString, connectionStringSettings.ConnectionString);
         Assert.AreEqual(config.ConnectionStrings.ConnectionStrings[connectionStringSettings.Name].CurrentConfiguration.FilePath, connectionStringSettings.CurrentConfiguration.FilePath);
      }

      /// <summary>Test stub for GetAlarmIntervals()</summary>
      [PexMethod]
      public Dictionary<int, int> GetAlarmIntervalsTest()
      {
         Dictionary<int, int> result = WebConfigurationReader.GetAlarmIntervals();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.GetAlarmIntervalsTest()
      }

      /// <summary>Test stub for GetAlarmIntervals(Configuration)</summary>
      [PexMethod]
      public Dictionary<int, int> GetAlarmIntervalsTest01(global::System.Configuration.Configuration config)
      {
         Dictionary<int, int> result = WebConfigurationReader.GetAlarmIntervals(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.GetAlarmIntervalsTest01(Configuration)
      }

      /// <summary>Test stub for GetConfiguration(String)</summary>
      [PexMethod]
      public global::System.Configuration.Configuration GetConfigurationTest(string filePath)
      {
         global::System.Configuration.Configuration result
            = WebConfigurationReader.GetConfiguration(filePath);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.GetConfigurationTest(String)
      }

      /// <summary>Test stub for GetConfiguration(FileInfo)</summary>
      [PexMethod]
      public global::System.Configuration.Configuration GetConfigurationTest01(FileInfo configFile)
      {
         global::System.Configuration.Configuration result
            = WebConfigurationReader.GetConfiguration(configFile);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.GetConfigurationTest01(FileInfo)
      }

      /// <summary>Test stub for ReadADAdminGroup()</summary>
      [PexMethod]
      public string ReadADAdminGroupTest()
      {
         string result = WebConfigurationReader.ReadADAdminGroup();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadADAdminGroupTest()
      }

      /// <summary>Test stub for ReadADAdminGroup(Configuration)</summary>
      [PexMethod]
      public string ReadADAdminGroupTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadADAdminGroup(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadADAdminGroupTest01(Configuration)
      }

      /// <summary>Test stub for ReadADRegularGroup()</summary>
      [PexMethod]
      public string ReadADRegularGroupTest()
      {
         string result = WebConfigurationReader.ReadADRegularGroup();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadADRegularGroupTest()
      }

      /// <summary>Test stub for ReadADRegularGroup(Configuration)</summary>
      [PexMethod]
      public string ReadADRegularGroupTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadADRegularGroup(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadADRegularGroupTest01(Configuration)
      }

      /// <summary>Test stub for ReadADViewerGroup(Configuration)</summary>
      [PexMethod]
      public string ReadADViewerGroupTest(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadADViewerGroup(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadADViewerGroupTest(Configuration)
      }

      /// <summary>Test stub for ReadADViewerGroup()</summary>
      [PexMethod]
      public string ReadADViewerGroupTest01()
      {
         string result = WebConfigurationReader.ReadADViewerGroup();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadADViewerGroupTest01()
      }

      /// <summary>Test stub for ReadConnectionString(String)</summary>
      [PexMethod]
      public string ReadConnectionStringTest(string ConnectionStringName)
      {
         string result
            = WebConfigurationReader.ReadConnectionString(ConnectionStringName);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadConnectionStringTest(String)
      }

      /// <summary>Test stub for ReadConnectionString(String, String)</summary>
      [PexMethod]
      public string ReadConnectionStringTest01(string ConnectionStringName, string webConfigFile)
      {
         string result = WebConfigurationReader.ReadConnectionString
                             (ConnectionStringName, webConfigFile);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadConnectionStringTest01(String, String)
      }

      /// <summary>Test stub for ReadLDAPConnectionSyncPath()</summary>
      [PexMethod]
      public string ReadLDAPConnectionSyncPathTest()
      {
         string result = WebConfigurationReader.ReadLDAPConnectionSyncPath();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadLDAPConnectionSyncPathTest()
      }

      /// <summary>Test stub for ReadLDAPConnectionSyncPath(Configuration)</summary>
      [PexMethod]
      public string ReadLDAPConnectionSyncPathTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadLDAPConnectionSyncPath(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadLDAPConnectionSyncPathTest01(Configuration)
      }

      /// <summary>Test stub for ReadLDAPDirectoryEntry()</summary>
      [PexMethod]
      public string ReadLDAPDirectoryEntryTest()
      {
         string result = WebConfigurationReader.ReadLDAPDirectoryEntry();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadLDAPDirectoryEntryTest()
      }

      /// <summary>Test stub for ReadLDAPDirectoryEntry(Configuration)</summary>
      [PexMethod]
      public string ReadLDAPDirectoryEntryTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadLDAPDirectoryEntry(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadLDAPDirectoryEntryTest01(Configuration)
      }

      /// <summary>Test stub for ReadProviderSettings(String, String)</summary>
      [PexMethod]
      public NameValueCollection ReadProviderSettingsTest(string ProviderName, string virtualPath)
      {
         NameValueCollection result
            = WebConfigurationReader.ReadProviderSettings(ProviderName, virtualPath);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadProviderSettingsTest(String, String)
      }

      /// <summary>Test stub for ReadProviderSettings(String, DirectoryInfo)</summary>
      [PexMethod]
      public NameValueCollection ReadProviderSettingsTest01(string ProviderName, DirectoryInfo dInfo)
      {
         NameValueCollection result
            = WebConfigurationReader.ReadProviderSettings(ProviderName, dInfo);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadProviderSettingsTest01(String, DirectoryInfo)
      }

      /// <summary>Test stub for ReadProviderSettings(String, FileInfo)</summary>
      [PexMethod]
      public NameValueCollection ReadProviderSettingsTest02(string ProviderName, FileInfo iInfo)
      {
         NameValueCollection result
            = WebConfigurationReader.ReadProviderSettings(ProviderName, iInfo);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadProviderSettingsTest02(String, FileInfo)
      }

      /// <summary>Test stub for ReadProviderSettings(Configuration, String)</summary>
      [PexMethod]
      public NameValueCollection ReadProviderSettingsTest03(global::System.Configuration.Configuration config, string ProviderName)
      {
         NameValueCollection result
            = WebConfigurationReader.ReadProviderSettings(config, ProviderName);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadProviderSettingsTest03(Configuration, String)
      }

      /// <summary>Test stub for ReadSmtpEnableSSL()</summary>
      [PexMethod]
      public string ReadSmtpEnableSSLTest()
      {
         string result = WebConfigurationReader.ReadSmtpEnableSSL();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpEnableSSLTest()
      }

      /// <summary>Test stub for ReadSmtpEnableSSL(Configuration)</summary>
      [PexMethod]
      public string ReadSmtpEnableSSLTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadSmtpEnableSSL(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpEnableSSLTest01(Configuration)
      }

      /// <summary>Test stub for ReadSmtpFromAddress()</summary>
      [PexMethod]
      public string ReadSmtpFromAddressTest()
      {
         string result = WebConfigurationReader.ReadSmtpFromAddress();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpFromAddressTest()
      }

      /// <summary>Test stub for ReadSmtpFromAddress(Configuration)</summary>
      [PexMethod]
      public string ReadSmtpFromAddressTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadSmtpFromAddress(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpFromAddressTest01(Configuration)
      }

      /// <summary>Test stub for ReadSmtpOpenRelay()</summary>
      [PexMethod]
      public string ReadSmtpOpenRelayTest()
      {
         string result = WebConfigurationReader.ReadSmtpOpenRelay();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpOpenRelayTest()
      }

      /// <summary>Test stub for ReadSmtpOpenRelay(Configuration)</summary>
      [PexMethod]
      public string ReadSmtpOpenRelayTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadSmtpOpenRelay(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpOpenRelayTest01(Configuration)
      }

      /// <summary>Test stub for ReadSmtpPassword()</summary>
      [PexMethod]
      public string ReadSmtpPasswordTest()
      {
         string result = WebConfigurationReader.ReadSmtpPassword();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpPasswordTest()
      }

      /// <summary>Test stub for ReadSmtpPassword(Configuration)</summary>
      [PexMethod]
      public string ReadSmtpPasswordTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadSmtpPassword(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpPasswordTest01(Configuration)
      }

      /// <summary>Test stub for ReadSmtpPort()</summary>
      [PexMethod]
      public string ReadSmtpPortTest()
      {
         string result = WebConfigurationReader.ReadSmtpPort();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpPortTest()
      }

      /// <summary>Test stub for ReadSmtpPort(Configuration)</summary>
      [PexMethod]
      public string ReadSmtpPortTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadSmtpPort(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpPortTest01(Configuration)
      }

      /// <summary>Test stub for ReadSmtpServer()</summary>
      [PexMethod]
      public string ReadSmtpServerTest()
      {
         string result = WebConfigurationReader.ReadSmtpServer();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpServerTest()
      }

      /// <summary>Test stub for ReadSmtpServer(Configuration)</summary>
      [PexMethod]
      public string ReadSmtpServerTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadSmtpServer(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadSmtpServerTest01(Configuration)
      }

      /// <summary>Test stub for ReadStoreCertificates()</summary>
      [PexMethod]
      public string ReadStoreCertificatesTest()
      {
         string result = WebConfigurationReader.ReadStoreCertificates();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadStoreCertificatesTest()
      }

      /// <summary>Test stub for ReadStoreCertificates(Configuration)</summary>
      [PexMethod]
      public string ReadStoreCertificatesTest01(global::System.Configuration.Configuration config)
      {
         string result = WebConfigurationReader.ReadStoreCertificates(config);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.ReadStoreCertificatesTest01(Configuration)
      }

      /// <summary>Test stub for SetADAdminGroup(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetADAdminGroupTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetADAdminGroup(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetADAdminGroupTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetADAdminGroup(Configuration, String)</summary>
      [PexMethod]
      public void SetADAdminGroupTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetADAdminGroup(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetADAdminGroupTest01(Configuration, String)
      }

      /// <summary>Test stub for SetADRegularGroup(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetADRegularGroupTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetADRegularGroup(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetADRegularGroupTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetADRegularGroup(Configuration, String)</summary>
      [PexMethod]
      public void SetADRegularGroupTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetADRegularGroup(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetADRegularGroupTest01(Configuration, String)
      }

      /// <summary>Test stub for SetADViewerGroup(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetADViewerGroupTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetADViewerGroup(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetADViewerGroupTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetADViewerGroup(Configuration, String)</summary>
      [PexMethod]
      public void SetADViewerGroupTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetADViewerGroup(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetADViewerGroupTest01(Configuration, String)
      }

      /// <summary>Test stub for SetAlarmIntervals(HttpRequestBase, Dictionary`2&lt;Int32,Int32&gt;)</summary>
      [PexMethod]
      public void SetAlarmIntervalsTest(HttpRequestBase CurrentRequest, Dictionary<int, int> intervals)
      {
         WebConfigurationReader.SetAlarmIntervals(CurrentRequest, intervals);
         // TODO: add assertions to method WebConfigurationReaderTest.SetAlarmIntervalsTest(HttpRequestBase, Dictionary`2<Int32,Int32>)
      }

      /// <summary>Test stub for SetAlarmIntervals(FileInfo, Dictionary`2&lt;Int32,Int32&gt;)</summary>
      [PexMethod]
      public void SetAlarmIntervalsTest01(FileInfo fInfo, Dictionary<int, int> intervals)
      {
         WebConfigurationReader.SetAlarmIntervals(fInfo, intervals);
         // TODO: add assertions to method WebConfigurationReaderTest.SetAlarmIntervalsTest01(FileInfo, Dictionary`2<Int32,Int32>)
      }

      /// <summary>Test stub for SetConnectionString(String, String, String)</summary>
      [PexMethod]
      public void SetConnectionStringTest(
          string webConfigFilePath,
          string ConnectionStringName,
          string value
      )
      {
         WebConfigurationReader.SetConnectionString
             (webConfigFilePath, ConnectionStringName, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetConnectionStringTest(String, String, String)
      }

      /// <summary>Test stub for SetConnectionString(HttpRequestBase, String, String)</summary>
      [PexMethod]
      public void SetConnectionStringTest01(
          HttpRequestBase CurrentRequest,
          string ConnectionStringName,
          string value
      )
      {
         WebConfigurationReader.SetConnectionString
             (CurrentRequest, ConnectionStringName, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetConnectionStringTest01(HttpRequestBase, String, String)
      }

      /// <summary>Test stub for SetConnectionString(Configuration, String, String)</summary>
      [PexMethod]
      public void SetConnectionStringTest02(
          global::System.Configuration.Configuration config,
          string ConnectionStringName,
          string value
      )
      {
         WebConfigurationReader.SetConnectionString(config, ConnectionStringName, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetConnectionStringTest02(Configuration, String, String)
      }

      /// <summary>Test stub for SetLDAPConnectionSyncPath(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetLDAPConnectionSyncPathTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetLDAPConnectionSyncPath(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetLDAPConnectionSyncPathTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetLDAPConnectionSyncPath(Configuration, String)</summary>
      [PexMethod]
      public void SetLDAPConnectionSyncPathTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetLDAPConnectionSyncPath(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetLDAPConnectionSyncPathTest01(Configuration, String)
      }

      /// <summary>Test stub for SetLDAPDirectoryEntry(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetLDAPDirectoryEntryTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetLDAPDirectoryEntry(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetLDAPDirectoryEntryTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetLDAPDirectoryEntry(Configuration, String)</summary>
      [PexMethod]
      public void SetLDAPDirectoryEntryTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetLDAPDirectoryEntry(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetLDAPDirectoryEntryTest01(Configuration, String)
      }

      /// <summary>Test stub for SetProviderSettings(HttpRequestBase, String, String, String)</summary>
      [PexMethod]
      public void SetProviderSettingsTest(
          HttpRequestBase CurrentRequest,
          string ProviderName,
          string keyname,
          string value
      )
      {
         WebConfigurationReader.SetProviderSettings
             (CurrentRequest, ProviderName, keyname, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetProviderSettingsTest(HttpRequestBase, String, String, String)
      }

      /// <summary>Test stub for SetProviderSettings(FileInfo, String, String, String)</summary>
      [PexMethod]
      public void SetProviderSettingsTest01(
          FileInfo fInfo,
          string ProviderName,
          string keyname,
          string value
      )
      {
         WebConfigurationReader.SetProviderSettings(fInfo, ProviderName, keyname, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetProviderSettingsTest01(FileInfo, String, String, String)
      }

      /// <summary>Test stub for SetProviderSettings(DirectoryInfo, String, String, String)</summary>
      [PexMethod]
      public void SetProviderSettingsTest02(
          DirectoryInfo dInfo,
          string ProviderName,
          string keyname,
          string value
      )
      {
         WebConfigurationReader.SetProviderSettings(dInfo, ProviderName, keyname, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetProviderSettingsTest02(DirectoryInfo, String, String, String)
      }

      /// <summary>Test stub for SetProviderSettings(Configuration, String, String, String)</summary>
      [PexMethod]
      public void SetProviderSettingsTest03(
          global::System.Configuration.Configuration config,
          string ProviderName,
          string keyname,
          string value
      )
      {
         WebConfigurationReader.SetProviderSettings(config, ProviderName, keyname, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetProviderSettingsTest03(Configuration, String, String, String)
      }

      /// <summary>Test stub for SetSmtpEnableSSL(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetSmtpEnableSSLTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetSmtpEnableSSL(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpEnableSSLTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetSmtpEnableSSL(Configuration, String)</summary>
      [PexMethod]
      public void SetSmtpEnableSSLTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetSmtpEnableSSL(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpEnableSSLTest01(Configuration, String)
      }

      /// <summary>Test stub for SetSmtpFromAddress(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetSmtpFromAddressTest(HttpRequestBase currentRequest, string value)
      {
         WebConfigurationReader.SetSmtpFromAddress(currentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpFromAddressTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetSmtpFromAddress(Configuration, String)</summary>
      [PexMethod]
      public void SetSmtpFromAddressTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetSmtpFromAddress(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpFromAddressTest01(Configuration, String)
      }

      /// <summary>Test stub for SetSmtpOpenRelay(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetSmtpOpenRelayTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetSmtpOpenRelay(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpOpenRelayTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetSmtpOpenRelay(Configuration, String)</summary>
      [PexMethod]
      public void SetSmtpOpenRelayTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetSmtpOpenRelay(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpOpenRelayTest01(Configuration, String)
      }

      /// <summary>Test stub for SetSmtpPassword(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetSmtpPasswordTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetSmtpPassword(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpPasswordTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetSmtpPassword(Configuration, String)</summary>
      [PexMethod]
      public void SetSmtpPasswordTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetSmtpPassword(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpPasswordTest01(Configuration, String)
      }

      /// <summary>Test stub for SetSmtpPort(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetSmtpPortTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetSmtpPort(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpPortTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetSmtpPort(Configuration, String)</summary>
      [PexMethod]
      public void SetSmtpPortTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetSmtpPort(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpPortTest01(Configuration, String)
      }

      /// <summary>Test stub for SetSmtpServer(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetSmtpServerTest(HttpRequestBase CurrentRequest, string value)
      {
         WebConfigurationReader.SetSmtpServer(CurrentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpServerTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetSmtpServer(Configuration, String)</summary>
      [PexMethod]
      public void SetSmtpServerTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetSmtpServer(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetSmtpServerTest01(Configuration, String)
      }

      /// <summary>Test stub for SetStoreCertificates(HttpRequestBase, String)</summary>
      [PexMethod]
      public void SetStoreCertificatesTest(HttpRequestBase currentRequest, string value)
      {
         WebConfigurationReader.SetStoreCertificates(currentRequest, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetStoreCertificatesTest(HttpRequestBase, String)
      }

      /// <summary>Test stub for SetStoreCertificates(Configuration, String)</summary>
      [PexMethod]
      public void SetStoreCertificatesTest01(global::System.Configuration.Configuration config, string value)
      {
         WebConfigurationReader.SetStoreCertificates(config, value);
         // TODO: add assertions to method WebConfigurationReaderTest.SetStoreCertificatesTest01(Configuration, String)
      }

      /// <summary>Test stub for UpdateAppSetting(HttpRequestBase, String, String)</summary>
      [PexMethod]
      public void UpdateAppSettingTest(
          HttpRequestBase currentRequest,
          string key,
          string value
      )
      {
         WebConfigurationReader.UpdateAppSetting(currentRequest, key, value);
         // TODO: add assertions to method WebConfigurationReaderTest.UpdateAppSettingTest(HttpRequestBase, String, String)
      }

      /// <summary>Test stub for UpdateAppSetting(String, String, String)</summary>
      [PexMethod]
      public void UpdateAppSettingTest01(
          string absoluteWebConfigPath,
          string key,
          string value
      )
      {
         WebConfigurationReader.UpdateAppSetting(absoluteWebConfigPath, key, value);
         // TODO: add assertions to method WebConfigurationReaderTest.UpdateAppSettingTest01(String, String, String)
      }

      /// <summary>Test stub for UpdateAppSetting(FileInfo, String, String)</summary>
      [PexMethod]
      public void UpdateAppSettingTest02(
          FileInfo fInfo,
          string key,
          string value
      )
      {
         WebConfigurationReader.UpdateAppSetting(fInfo, key, value);
         // TODO: add assertions to method WebConfigurationReaderTest.UpdateAppSettingTest02(FileInfo, String, String)
      }

      /// <summary>Test stub for UpdateAppSetting(DirectoryInfo, String, String)</summary>
      [PexMethod]
      public void UpdateAppSettingTest03(
          DirectoryInfo dInfo,
          string key,
          string value
      )
      {
         WebConfigurationReader.UpdateAppSetting(dInfo, key, value);
         // TODO: add assertions to method WebConfigurationReaderTest.UpdateAppSettingTest03(DirectoryInfo, String, String)
      }

      /// <summary>Test stub for UpdateAppSetting(Configuration, String, String)</summary>
      [PexMethod]
      public void UpdateAppSettingTest04(
          global::System.Configuration.Configuration configuration,
          string key,
          string value
      )
      {
         WebConfigurationReader.UpdateAppSetting(configuration, key, value);
         // TODO: add assertions to method WebConfigurationReaderTest.UpdateAppSettingTest04(Configuration, String, String)
      }


      /// <summary>Test stub for GetConfiguration(HttpRequestBase, String)</summary>
      [PexMethod]
      public global::System.Configuration.Configuration GetConfigurationTest02(HttpRequestBase currentRequest, string virtualPath)
      {
         global::System.Configuration.Configuration result
            = WebConfigurationReader.GetConfiguration(currentRequest, virtualPath);
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.GetConfigurationTest02(HttpRequestBase, String)
      }

      /// <summary>Test stub for GetConfiguration()</summary>
      [PexMethod]
      public global::System.Configuration.Configuration GetConfigurationTest03()
      {
         global::System.Configuration.Configuration result = WebConfigurationReader.GetConfiguration();
         return result;
         // TODO: add assertions to method WebConfigurationReaderTest.GetConfigurationTest03()
      }
   }
}
