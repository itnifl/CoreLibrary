﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CoreLibrary.Common
{
   public static class CopyHelper
   {
      public static T DeepCopySerializable<T>(T other) {
         using (MemoryStream ms = new MemoryStream()) {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(ms, other);
            ms.Position = 0;
            return (T)formatter.Deserialize(ms);
         }
      }
   }
}