﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Configuration;
using System.Collections.Specialized;
using System.IO;
using CoreLibrary.Common;
using System.Web.Hosting;
using CoreLibrary.Helpers;

namespace CoreLibrary.Common {
    public static class WebConfigurationReader {
        public static Configuration CurrentConfiguration
        {
            get; 
            set;
        }
        /// <summary> 
        /// Encrypt web.config connectionStrings
        /// section using Rsa protected configuration
        /// provider model 
        /// </summary> 
        #region Encrypt method 
        public static void EncryptConnectionStrings(HttpRequestBase currentRequest) {
            Configuration config = null;
            if (currentRequest != null) {
                string path = currentRequest.ApplicationPath;
                config = WebConfigurationManager.OpenWebConfiguration(path);
            } else {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }

            ConfigurationSection section = config.GetSection("connectionStrings");

            if (!section.SectionInformation.IsProtected) {
                section.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                SaveConfig(config);
            }
        }
        #endregion
        /// <summary> 
        /// Decrypts connectionStrings section in 
        ///web.config using Rsa provider model 
        /// </summary> 
        #region Decrypt method 
        public static void DecryptConnStrings(HttpRequestBase currentRequest)
        {
            Configuration config = null;
            if (currentRequest != null)
            {
                string path = currentRequest.ApplicationPath;
                config = WebConfigurationManager.OpenWebConfiguration(path);
            } else
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }

            ConfigurationSection section = config.GetSection("connectionStrings");

            if (section.SectionInformation.IsProtected) {
                section.SectionInformation.UnprotectSection();
                SaveConfig(config);
            }
        }
        #endregion    
        public static void EncryptConfigSection(HttpRequestBase currentRequest, string sectionKey) {
            Configuration config = null;
            if (currentRequest != null) {
                string path = currentRequest.ApplicationPath;
                config = WebConfigurationManager.OpenWebConfiguration(path);
            } else {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            ConfigurationSection section = config.GetSection(sectionKey);
            if (section != null && !section.SectionInformation.IsProtected && !section.ElementInformation.IsLocked) {
                section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                section.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Full);
            }
        }
        public static bool IsEncrypted(HttpRequestBase currentRequest, string sectionKey) {
            Configuration config = null;
            if (currentRequest != null) {
                string path = currentRequest.ApplicationPath;
                config = WebConfigurationManager.OpenWebConfiguration(path);
            } else {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            ConfigurationSection section = config.GetSection(sectionKey);
            return section != null && section.SectionInformation.IsProtected;
        }
        public static string ReadConnectionString(string ConnectionStringName) {
            return ConfigurationManager.ConnectionStrings[ConnectionStringName]?.ConnectionString ?? "";
        }
        public static string ReadConnectionString(string ConnectionStringName, string webConfigFile) {
            var config = GetConfiguration(webConfigFile);
            return config.ConnectionStrings.ConnectionStrings[ConnectionStringName]?.ConnectionString ?? "";
        }

        private static string GetWebConfigPath(string ApplicationPath) {
            if (String.IsNullOrEmpty(ApplicationPath)) {
                ApplicationPath = String.Empty;
            }
            List<Exception> aExceptions = new List<Exception>();
            string result = String.Empty;
            try {
                result = System.IO.Path.Combine(HttpContext.Current?.Server?.MapPath(ApplicationPath) ?? 
                    System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ApplicationPath.Replace(@"~\", "").Replace(@"~/", ""), "Web.config"));
            } catch (Exception ex1) {
                aExceptions.Add(ex1);
                try {
                    result = Path.Combine(Environment.CurrentDirectory, ApplicationPath, "Web.config");
                } catch (Exception ex2) {
                    aExceptions.Add(ex2);
                    throw new AggregateException("Could not determine the path to the Web.config file.", aExceptions);
                }
            }
            return result;
        }
        public static void SetConnectionString(string webConfigFilePath, string ConnectionStringName, string value) {
            Configuration config = GetConfiguration(webConfigFilePath);
            SetConnectionString(config, ConnectionStringName, value);
        }
        public static void SetConnectionString(HttpRequestBase CurrentRequest, string ConnectionStringName, string value) {
            Configuration config = GetConfiguration(CurrentRequest);
            SetConnectionString(config, ConnectionStringName, value);
        }
        public static void SetConnectionString(Configuration config, string ConnectionStringName, string value) {
            if (config.ConnectionStrings.ConnectionStrings[ConnectionStringName] != null) {
                config.ConnectionStrings.ConnectionStrings[ConnectionStringName].ConnectionString = value;
                SaveConfig(config, ConfigurationSaveMode.Modified, "connectionStrings");
            } else {
                AddAndSaveOneConnectionStringSettings(config, new ConnectionStringSettings() {
                    ConnectionString = value,
                    Name = ConnectionStringName,
                    ProviderName = "System.Data.SqlClient"
                });
            }
        }
        public static void AddAndSaveOneConnectionStringSettings(
         System.Configuration.Configuration configuration,
         System.Configuration.ConnectionStringSettings connectionStringSettings) {
            if (configuration == null) {
                configuration = GetConfiguration();
            }
            if (connectionStringSettings == null) {
                connectionStringSettings = new ConnectionStringSettings() {
                    ConnectionString = configuration.ConnectionStrings.ConnectionStrings[0].ConnectionString,
                    Name = configuration.ConnectionStrings.ConnectionStrings[0].Name,
                    ProviderName = configuration.ConnectionStrings.ConnectionStrings[0].ProviderName
                };
            }
            // You cannot add to ConfigurationManager.ConnectionStrings using
            // ConfigurationManager.ConnectionStrings.Add
            // (connectionStringSettings) -- This fails.

            // But you can add to the configuration section and refresh the ConfigurationManager.

            // Get the connection strings section; Even if it is in another file.
            ConnectionStringsSection connectionStringsSection = configuration.ConnectionStrings;

            // Add the new element to the section.
            connectionStringsSection.ConnectionStrings.Add(connectionStringSettings);

            // Save the configuration file.//System.Configuration.ConfigurationErrorsException
            SaveConfig(configuration, ConfigurationSaveMode.Minimal, "connectionStrings");
        }

        public static NameValueCollection ReadProviderSettings(string ProviderName, String virtualPath) {
            // Get the Web application configuration object.         
            Configuration config = GetConfiguration(null, virtualPath);

            // Get the section related object.
            return ReadProviderSettings(config, ProviderName);
        }
        public static NameValueCollection ReadProviderSettings(string ProviderName, DirectoryInfo dInfo) {
            // Get the Web application configuration object.         
            Configuration config = GetConfiguration(System.IO.Path.Combine(dInfo.ToString(), "web.config"));            

            // Get the section related object.
            return ReadProviderSettings(config, ProviderName);
        }
        public static NameValueCollection ReadProviderSettings(string ProviderName, string virtualPath, FileInfo iInfo) {
            // Get the Web application configuration object.                     
            using (AppConfig.Change(iInfo.ToString())) {
                Configuration config = GetConfiguration(null, virtualPath);
                return ReadProviderSettings(config, ProviderName);
            }
            // Get the section related object.
            
        }
        public static NameValueCollection ReadProviderSettings(Configuration config, string ProviderName) {
            MembershipSection configSection = (MembershipSection)config.GetSection("system.web/membership") ?? (MembershipSection)WebConfigurationManager.GetSection("system.web/membership");
            foreach (ProviderSettings ps in configSection.Providers) {
                if (ps.Name == ProviderName) {
                    return ps.Parameters;
                }
            }
            return null;
        }
        public static void SetProviderSettings(HttpRequestBase CurrentRequest, string ProviderName, string keyname,
           string value) {
            Configuration config = GetConfiguration(CurrentRequest);
            // Get the section related object.
            SetProviderSettings(config, ProviderName, keyname, value);
        }
        public static void SetProviderSettings(FileInfo fInfo, string ProviderName, string keyname,
           string value) {
            Configuration config = GetConfiguration(fInfo.ToString());
            // Get the section related object.
            SetProviderSettings(config, ProviderName, keyname, value);
        }
        public static void SetProviderSettings(DirectoryInfo dInfo, string ProviderName, string keyname,
           string value) {
            Configuration config = GetConfiguration(System.IO.Path.Combine(dInfo.ToString(), "web.config"));
            // Get the section related object.
            SetProviderSettings(config, ProviderName, keyname, value);
        }
        public static void SetProviderSettings(Configuration config, string ProviderName, string keyname,
           string value) {
            MembershipSection configSection = (MembershipSection)config.GetSection("system.web/membership");

            foreach (ProviderSettings ps in configSection.Providers) {
                if (ps.Name == ProviderName) {
                    ps.Parameters.Set(keyname, value);
                }
            }
            SaveConfig(config, ConfigurationSaveMode.Modified, "system.web/membership");
        }
        public static void UpdateAppSetting(HttpRequestBase currentRequest, string key, string value) {
            Configuration configuration = GetConfiguration(currentRequest);
            UpdateAppSetting(configuration, key, value);
        }
        public static void UpdateAppSetting(string absoluteWebConfigPath, string key, string value) {
            Configuration configuration = GetConfiguration(absoluteWebConfigPath);
            UpdateAppSetting(configuration, key, value);
        }
        public static void UpdateAppSetting(FileInfo fInfo, string key, string value) {
            Configuration configuration = GetConfiguration(fInfo.ToString());
            UpdateAppSetting(configuration, key, value);
        }
        public static void UpdateAppSetting(DirectoryInfo dInfo, string key, string value) {
            Configuration configuration = GetConfiguration(System.IO.Path.Combine(dInfo.ToString(), "web.config"));
            UpdateAppSetting(configuration, key, value);
        }
        public static void UpdateAppSetting(Configuration configuration, string key, string value) {
            if (configuration.AppSettings.Settings[key] == null) {
                configuration.AppSettings.Settings.Add(key, value);
            }
            configuration.AppSettings.Settings[key].Value = value;
            SaveConfig(configuration, "appSettings");
        }
        private static void SaveConfig(Configuration configuration, ConfigurationSaveMode mode, string refreshSection = "")
        {
            configuration.Save(mode, true);
            if (!String.IsNullOrEmpty(refreshSection))
                ConfigurationManager.RefreshSection(refreshSection);
            
            CleanConfig(configuration);
        }
        private static void SaveConfig(Configuration configuration, string refreshSection = "")
        {
            configuration.Save();
            if(!String.IsNullOrEmpty(refreshSection))
                ConfigurationManager.RefreshSection(refreshSection);
            
            CleanConfig(configuration);
        }
        private static void CleanConfig(Configuration configuration)
        {
            var fileContent = File.ReadAllLines(configuration.FilePath);
            fileContent = fileContent.Where(x => !x.Contains("ApplicationConfigurationSection")).ToArray();
            File.WriteAllLines(configuration.FilePath, fileContent);
        }
        /// <summary>
        /// Returns web.config specified in argument
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static Configuration GetConfiguration(string physicalPath) {
            string dummyVirtualPath = "/MyApp";
            WebConfigurationFileMap map = new WebConfigurationFileMap();
            map.VirtualDirectories.Add(dummyVirtualPath, new VirtualDirectoryMapping(physicalPath, true));
            Configuration configuration = WebConfigurationManager.OpenMappedWebConfiguration(map, dummyVirtualPath);
            CurrentConfiguration = configuration;
            return configuration;
        }
        public static Configuration GetConfiguration(FileInfo configFile) {
            var vdm = new VirtualDirectoryMapping(configFile.DirectoryName, true, configFile.Name);
            var wcfm = new WebConfigurationFileMap(configFile.Name);
            wcfm.VirtualDirectories.Add("/", vdm);
            var configuration = WebConfigurationManager.OpenMappedWebConfiguration(wcfm, "/");
            CurrentConfiguration = configuration;
            return configuration;
        }
        /// <summary>
        /// Returns web.config based on  HttpRequestBase and a virtual path
        /// </summary>
        /// <param name="currentRequest"></param>
        /// <param name="virtualPath">The folder or folder structure where to look</param>
        /// <returns></returns>
        public static Configuration GetConfiguration(HttpRequestBase currentRequest, String virtualPath = "") {
            Configuration config = null;
            try {
                try {
                    try
                    {
                        config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(
                            String.IsNullOrEmpty(virtualPath) ? "~" : virtualPath);
                    } catch
                    {
                        throw;
                        /* This code lets you use the APP_CONFIG_FILE instead:
                        var configFile = new FileInfo(AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString());
                        var vdm = new VirtualDirectoryMapping(configFile.DirectoryName, true, configFile.Name);
                        var wcfm = new WebConfigurationFileMap();
                        wcfm.VirtualDirectories.Add("/", vdm);
                        config = WebConfigurationManager.OpenMappedWebConfiguration(wcfm, "/");
                        */
                    }
                } catch {
                    if ((currentRequest == null || !System.IO.Directory.Exists(currentRequest.ApplicationPath)) && !HostingEnvironment.IsHosted) {
                        string strFolder = String.Empty;
                        if (Directory.Exists(Path.Combine(System.IO.Directory.GetCurrentDirectory(), "Configs"))) {
                            strFolder = Path.Combine(System.IO.Directory.GetCurrentDirectory(), "Configs");
                        } else {
                            strFolder = System.IO.Directory.GetCurrentDirectory();
                        }
                        string destFile = Path.Combine(strFolder, "Web.config");
                        if (!File.Exists(destFile)) {
                            File.Create(destFile);
                        }
                        System.Configuration.ConfigurationFileMap fileMap = new ConfigurationFileMap(destFile);
                        config = System.Configuration.ConfigurationManager.OpenMappedMachineConfiguration(fileMap);
                    } else
                    {
                        throw;
                    }
                }
            } catch {
                if (currentRequest != null && !String.IsNullOrEmpty(currentRequest.ApplicationPath))
                {
                    var configFile = new FileInfo(GetWebConfigPath(currentRequest.ApplicationPath));
                    var vdm = new VirtualDirectoryMapping(configFile.DirectoryName, true, configFile.Name);
                    var wcfm = new WebConfigurationFileMap();
                    wcfm.VirtualDirectories.Add("/", vdm);
                    config = WebConfigurationManager.OpenMappedWebConfiguration(wcfm, "/");
                } else
                {
                    config = WebConfigurationManager.OpenWebConfiguration("/");
                }
            }
            CurrentConfiguration = config;
            return config;
        }
        /// <summary>
        /// Fetches configuration from root folder
        /// </summary>
        /// <returns></returns>
        public static Configuration GetConfiguration() {
            Configuration config = null;
            if (HostingEnvironment.IsHosted) {
                config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            } else {
                config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/");
            }
            if (config == null) {
                config = System.Configuration.ConfigurationManager.OpenExeConfiguration(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            }
            CurrentConfiguration = config;
            return config;
        }

        public static string ReadAppSetting(Configuration config, string settingKey) {
            string answer = String.Empty;
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                answer = appSettings?.Settings[settingKey].ToString() ?? String.Empty;
            } catch (Exception) {
                //Can be unhandled
            }
            return answer;
        }
        public static string ReadAppSetting(string settingKey) {
            string answer = String.Empty;
            try {
                answer = System.Configuration.ConfigurationManager.AppSettings[settingKey]?.ToString() ?? String.Empty;
            } catch (Exception) {
                //Can be unhandled
            }
            return answer;
        }
    }
    public static class SettingsReader {
        #region Specific Methods      
        public static string ReadADViewerGroup(Configuration config) {
            string ADViewerGroup = "WEBReader-Default";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                ADViewerGroup = appSettings?.Settings["ADViewerGroup"].ToString() ?? "WEBViewer-Default";
                if (String.IsNullOrEmpty(ADViewerGroup)) {
                    ADViewerGroup = "WEBReader-Default";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return ADViewerGroup;
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadADViewerGroup() {
            string ADViewerGroup = "WEBReader-Default";
            try {
                ADViewerGroup = System.Configuration.ConfigurationManager.AppSettings["ADViewerGroup"]?.ToString() ?? "WEBViewer-Default";
                if (String.IsNullOrEmpty(ADViewerGroup)) {
                    ADViewerGroup = "WEBReader-Default";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return ADViewerGroup;
        }
        public static void SetADViewerGroup(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "ADViewerGroup", value);
        }
        public static void SetADViewerGroup(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "ADViewerGroup", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadADRegularGroup() {
            string ADRegularGroup = "WEBRegular-Default";
            try {
                ADRegularGroup = System.Configuration.ConfigurationManager.AppSettings["ADRegularGroup"]?.ToString() ?? "WEBRegular-Default";
                if (String.IsNullOrEmpty(ADRegularGroup)) {
                    ADRegularGroup = "WEBRegular-Default";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return ADRegularGroup;
        }
        public static string ReadADRegularGroup(Configuration config) {
            string ADRegularGroup = "WEBRegular-Default";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                ADRegularGroup = appSettings?.Settings["ADRegularGroup"].ToString() ?? "WEBRegular-Default";
                if (String.IsNullOrEmpty(ADRegularGroup)) {
                    ADRegularGroup = "WEBRegular-Default";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return ADRegularGroup;
        }
        public static void SetADRegularGroup(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "ADRegularGroup", value);
        }
        public static void SetADRegularGroup(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "ADRegularGroup", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadADAdminGroup(string strConfigPath = "") {
            string ADAdminGroup = "WEBAdmin-Default";
            try {
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath))
                {
                    ADAdminGroup = System.Configuration.ConfigurationManager.AppSettings["ADAdminGroup"]?.ToString() ?? "WEBAdmin-Default";
                }                
                if (String.IsNullOrEmpty(ADAdminGroup)) {
                    ADAdminGroup = "WEBAdmin-Default";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return ADAdminGroup;
        }
        public static string ReadADAdminGroup(Configuration config) {
            string ADAdminGroup = "WEBAdmin-Default";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;                
                ADAdminGroup = appSettings?.Settings["ADAdminGroup"].ToString() ?? "WEBAdmin-Default";                
                
                if (String.IsNullOrEmpty(ADAdminGroup)) {
                    ADAdminGroup = "WEBAdmin-Default";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return ADAdminGroup;
        }

        public static void SetADAdminGroup(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "ADAdminGroup", value);
        }
        public static void SetADAdminGroup(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "ADAdminGroup", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadSmtpServer(string strConfigPath = "") {
            string smtpServer = "smtp.unknown.com";
            try {
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath)) {
                    smtpServer = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"]?.ToString() ?? String.Empty;
                }                
                if (String.IsNullOrEmpty(smtpServer)) {
                    smtpServer = "smtp.unknown.com";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return smtpServer;
        }
        public static string ReadSmtpServer(Configuration config) {
            string smtpServer = "smtp.unknown.com";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                smtpServer = appSettings?.Settings["SmtpServer"]?.Value ?? String.Empty;
                if (String.IsNullOrEmpty(smtpServer)) {
                    smtpServer = "smtp.unknown.com";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return smtpServer;
        }

        public static void SetSmtpServer(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "SmtpServer", value);
        }
        public static void SetSmtpServer(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "SmtpServer", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadSmtpFromAddress(string strConfigPath = "") {
            string value = "noreply@noaddress.com";
            try {                
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath)) {
                    value = System.Configuration.ConfigurationManager.AppSettings["FromAddress"]?.ToString() ?? String.Empty;
                }
                if (String.IsNullOrEmpty(value)) {
                    value = "noreply@noaddress.com";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }
        public static string ReadSmtpFromAddress(Configuration config) {
            string value = "noreply@noaddress.com";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                value = appSettings?.Settings["FromAddress"]?.Value ?? String.Empty;
                if (String.IsNullOrEmpty(value)) {
                    value = "noreply@noaddress.com";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }

        public static void SetSmtpFromAddress(HttpRequestBase currentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(currentRequest, "FromAddress", value);
        }
        public static void SetSmtpFromAddress(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "FromAddress", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadStoreCertificates(string strConfigPath = "") {
            string value = "Yes";
            try {
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath)) {
                    value = System.Configuration.ConfigurationManager.AppSettings["StoreCertificates"]?.ToString() ?? "Yes";
                }                
                if (String.IsNullOrEmpty(value)) {
                    value = "Yes";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }
        public static string ReadStoreCertificates(Configuration config) {
            string value = "Yes";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                value = appSettings?.Settings["StoreCertificates"]?.Value ?? "Yes";
                if (String.IsNullOrEmpty(value)) {
                    value = "Yes";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }

        public static void SetStoreCertificates(HttpRequestBase currentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(currentRequest, "StoreCertificates", value);
        }
        public static void SetStoreCertificates(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "StoreCertificates", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadSmtpOpenRelay(string strConfigPath = "") {
            string value = "Yes";
            try {
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath)) {
                    value = System.Configuration.ConfigurationManager.AppSettings["OpenRelay"]?.ToString() ?? "Yes";
                }                
                if (String.IsNullOrEmpty(value)) {
                    value = "Yes";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }
        public static string ReadSmtpOpenRelay(Configuration config) {
            string value = "Yes";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                value = appSettings?.Settings["OpenRelay"]?.Value ?? "Yes";
                if (String.IsNullOrEmpty(value)) {
                    value = "Yes";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }

        public static void SetSmtpOpenRelay(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "OpenRelay", value);
        }
        public static void SetSmtpOpenRelay(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "OpenRelay", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadSmtpPassword(string strConfigPath = "") {
            string value = "Password not set";
            try { 
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath)) {
                    value = System.Configuration.ConfigurationManager.AppSettings["Password"]?.ToString() ?? String.Empty;
                }
            
                if (String.IsNullOrEmpty(value)) {
                    value = "Password not set";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }
        public static string ReadSmtpPassword(Configuration config) {
            string value = "Password not set";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                value = appSettings?.Settings["Password"]?.Value ?? String.Empty;
                if (String.IsNullOrEmpty(value)) {
                    value = "Password not set";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }

        public static void SetSmtpEnableSSL(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "EnableSSL", value);
        }
        public static void SetSmtpEnableSSL(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "EnableSSL", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadSmtpEnableSSL(string strConfigPath = "") {
            string value = "No";
            try {
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath)) {
                    value = System.Configuration.ConfigurationManager.AppSettings["EnableSSL"]?.ToString() ?? "No";
                }                
                if (String.IsNullOrEmpty(value)) {
                    value = "No";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }
        public static string ReadSmtpEnableSSL(Configuration config) {
            string value = "No";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                value = appSettings?.Settings["EnableSSL"]?.Value ?? "No";
                if (String.IsNullOrEmpty(value)) {
                    value = "No";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }

        public static void SetSmtpPort(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "SmtpPort", value);
        }
        public static void SetSmtpPort(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "SmtpPort", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadSmtpPort(string strConfigPath = "") {
            string value = "25";
            try {
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath)) {
                    value = System.Configuration.ConfigurationManager.AppSettings["SmtpPort"]?.ToString() ?? String.Empty;
                }                
                if (String.IsNullOrEmpty(value)) {
                    value = "25";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }
        public static string ReadSmtpPort(Configuration config) {
            string value = "25";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                value = appSettings?.Settings["SmtpPort"]?.ToString() ?? String.Empty;
                if (String.IsNullOrEmpty(value)) {
                    value = "25";
                }
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }

        public static void SetSmtpPassword(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "Password", value);
        }
        public static void SetSmtpPassword(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "Password", value);
        }
        public static void SetLDAPDirectoryEntry(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "LDAPDirectoryEntry", value);
        }
        public static void SetLDAPDirectoryEntry(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "LDAPDirectoryEntry", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadLDAPDirectoryEntry(string strConfigPath) {
            string value = "";
            try {
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath)) {
                    value = System.Configuration.ConfigurationManager.AppSettings["LDAPDirectoryEntry"]?.ToString() ?? String.Empty;
                }                
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }
        public static string ReadLDAPDirectoryEntry(Configuration config) {
            string value = "";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                value = appSettings?.Settings["LDAPDirectoryEntry"]?.ToString() ?? String.Empty;
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }

        public static void SetLDAPConnectionSyncPath(HttpRequestBase CurrentRequest, string value) {
            WebConfigurationReader.UpdateAppSetting(CurrentRequest, "LDAPConnectionSyncPath", value);
        }
        public static void SetLDAPConnectionSyncPath(Configuration config, string value) {
            WebConfigurationReader.UpdateAppSetting(config, "LDAPConnectionSyncPath", value);
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSetting
        /// </summary>
        /// <returns></returns>
        public static string ReadLDAPConnectionSyncPath(string strConfigPath = "") {
            string value = "";
            try {
                using (AppConfig.Change(String.IsNullOrEmpty(strConfigPath) ? AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString() : strConfigPath)) {
                    value = System.Configuration.ConfigurationManager.AppSettings["LDAPConnectionSyncPath"]?.ToString() ?? String.Empty;
                }                
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }
        public static string ReadLDAPConnectionSyncPath(Configuration config) {
            string value = "";
            try {
                var appSettings = (AppSettingsSection)config.AppSettings;
                value = appSettings?.Settings["LDAPConnectionSyncPath"]?.ToString() ?? String.Empty;
            } catch (Exception) {
                //Can be unhandled
            }
            return value;
        }

        public static void SetAlarmIntervals(HttpRequestBase CurrentRequest, Dictionary<int, int> intervals) {
            int alarmInterval;
            if (intervals.TryGetValue(1, out alarmInterval))
                WebConfigurationReader.UpdateAppSetting(CurrentRequest, "CertificateWarningLevel1", alarmInterval.ToString());

            if (intervals.TryGetValue(2, out alarmInterval))
                WebConfigurationReader.UpdateAppSetting(CurrentRequest, "CertificateWarningLevel2", alarmInterval.ToString());

            if (intervals.TryGetValue(3, out alarmInterval))
                WebConfigurationReader.UpdateAppSetting(CurrentRequest, "CertificateWarningLevel3", alarmInterval.ToString());

            if (intervals.TryGetValue(4, out alarmInterval))
                WebConfigurationReader.UpdateAppSetting(CurrentRequest, "CertificateWarningLevel4", alarmInterval.ToString());
        }
        public static void SetAlarmIntervals(FileInfo fInfo, Dictionary<int, int> intervals) {
            int alarmInterval;
            if (intervals.TryGetValue(1, out alarmInterval))
                WebConfigurationReader.UpdateAppSetting(fInfo, "CertificateWarningLevel1", alarmInterval.ToString());

            if (intervals.TryGetValue(2, out alarmInterval))
                WebConfigurationReader.UpdateAppSetting(fInfo, "CertificateWarningLevel2", alarmInterval.ToString());

            if (intervals.TryGetValue(3, out alarmInterval))
                WebConfigurationReader.UpdateAppSetting(fInfo, "CertificateWarningLevel3", alarmInterval.ToString());

            if (intervals.TryGetValue(4, out alarmInterval))
                WebConfigurationReader.UpdateAppSetting(fInfo, "CertificateWarningLevel4", alarmInterval.ToString());
        }
        /// <summary>
        /// Uses the default configuration files to read the AppSettings
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, int> GetAlarmIntervals(string strConfigPath = "")
        {
            System.Configuration.Configuration configuration = null;

            string strCertificateWarningLevel1 = String.Empty;
            string strCertificateWarningLevel2 = String.Empty;
            string strCertificateWarningLevel3 = String.Empty;
            string strCertificateWarningLevel4 = String.Empty;

            if (String.IsNullOrEmpty(strConfigPath))
            {
                strConfigPath = AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString();

            } 
            using (AppConfig.Change(strConfigPath)) {
                // tempFileName is used for the app config during this context
                strCertificateWarningLevel1 = System.Configuration.ConfigurationManager
                                                    .AppSettings["CertificateWarningLevel1"]
                                                    ?.ToString() ?? "30";
                strCertificateWarningLevel2 = System.Configuration.ConfigurationManager
                                                    .AppSettings["CertificateWarningLevel2"]
                                                    ?.ToString() ?? "20";
                strCertificateWarningLevel3 = System.Configuration.ConfigurationManager
                                                    .AppSettings["CertificateWarningLevel3"]
                                                    ?.ToString() ?? "7";
                strCertificateWarningLevel4 = System.Configuration.ConfigurationManager
                                                    .AppSettings["CertificateWarningLevel4"]
                                                    ?.ToString() ?? "0";
            }                                 
            return GetAlarmIntervals(strCertificateWarningLevel1, strCertificateWarningLevel2, strCertificateWarningLevel3, strCertificateWarningLevel4);
        }
        public static Dictionary<int, int> GetAlarmIntervals(Configuration config) {
            var appSettings = config.AppSettings;

            string strCertificateWarningLevel1 = appSettings?.Settings["CertificateWarningLevel1"]?.ToString() ?? "30";
            string strCertificateWarningLevel2 = appSettings?.Settings["CertificateWarningLevel2"]?.ToString() ?? "20";
            string strCertificateWarningLevel3 = appSettings?.Settings["CertificateWarningLevel3"]?.ToString() ?? "7";
            string strCertificateWarningLevel4 = appSettings?.Settings["CertificateWarningLevel4"]?.ToString() ?? "0";

            return GetAlarmIntervals(strCertificateWarningLevel1, strCertificateWarningLevel2, strCertificateWarningLevel3, strCertificateWarningLevel4);
        }
        private static Dictionary<int, int> GetAlarmIntervals(string strCertificateWarningLevel1, 
            string strCertificateWarningLevel2, 
            string strCertificateWarningLevel3, 
            string strCertificateWarningLevel4) {
            Dictionary<int, int> AlarmLevels = new Dictionary<int, int>();

            int warning1, warning2, critical, error;
            Int32.TryParse(strCertificateWarningLevel1, out warning1);
            Int32.TryParse(strCertificateWarningLevel2, out warning2);
            Int32.TryParse(strCertificateWarningLevel3, out critical);
            Int32.TryParse(strCertificateWarningLevel4, out error);

            AlarmLevels.Add(1, warning1);
            AlarmLevels.Add(2, warning2);
            AlarmLevels.Add(3, critical);
            AlarmLevels.Add(4, error);

            return AlarmLevels;
        }
        #endregion
    }
}